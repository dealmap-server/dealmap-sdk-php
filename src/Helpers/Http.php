<?php

namespace Dealmap\Phpsdk\Helpers;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Http as HttpClient;
use Illuminate\Support\Facades\Log;

trait Http
{
    protected function get($url, $params, $domain = "platform")
    {
        $domain = $this->getDomain($domain);
        $params = $this->resetParams($params);
        $response = HttpClient::get($domain . $url, $params);
        return $this->getData($response);
    }

    protected function post($url, $params, $domain = "platform")
    {
        $domain = $this->getDomain($domain);
        $params = $this->resetParams($params);
        $response = HttpClient::post($domain . $url, $params);
        return $this->getData($response);
    }

    protected function refreshToken()
    {
        if (!method_exists($this, 'setToken')) {
            throw new Exception('请定义setToken方法');
        }
        $appid = env('APP_ID');
        $secret = env('APP_SECRET');
        $domain = $this->getPlatformDomain();
        $response = HttpClient::post($domain . 'app/token', ['appid' => $appid, 'secret' => $secret]);
        $token = $this->getData($response);
        $this->setToken($token);
    }

    private function resetParams($params)
    {
        if (!method_exists($this, 'getToken')) {
            throw new Exception('请定义getToken方法');
        }
        $params['access_token'] = $this->getToken();
        $params['timestamp'] = Carbon::now()->timestamp;
        ksort($params);
        $query = http_build_query($params);
        $params['sign'] = password_hash($query . env('SIGN_KEY'), PASSWORD_BCRYPT);
        return $params;
    }

    private function getDomain($domain)
    {
        switch ($domain) {
            case "fee":
                return $this->getFeeDomain();
            default:
                return $this->getPlatformDomain();
        }
    }

    private function getFeeDomain()
    {
        switch (env('APP_ENV')) {
            case 'local':
                return env('FEE_DOMAIN');
            case 'dev':
                return 'https://fee-api.dev2.dealmap.cloud/';
            case 'pre':
                return 'https://fee-api.pre.dealmap.cloud/';
            default:
                return 'https://fee-api.dealmap.cloud/';
        }
    }

    private function getPlatformDomain()
    {
        switch (env('APP_ENV')) {
            case 'local':
                return env('PLATFORM_DOMAIN');
            case 'dev':
                return 'https://platform-api.dev2.dealmap.cloud/';
            case 'pre':
                return 'https://platform-api.pre.dealmap.cloud/';
            default:
                return 'https://platform-api.dealmap.cloud/';
        }
    }

    private function getData($response)
    {
        $json = $response->json();
        if ($json['status'] !== 200) {
            throw new Exception($json['message'], $json['status']);
        }
        return $json['data'];
    }
}
